# Tools for Editing Articles in Lazy Wiki

(Lazy Wiki is my private wiki software)

## Bullet

* Save a list as a file, and bullet.py will format it for LazyWiki
* Add "* " to every line and remove/replace strings
* Perfect for long lists with undesired characters (extra spaces, punctuation, links, etc)

# Runs in Python 2.7
