## File with unformatted article
oldFile = 'article.txt' 

## Output for formatted article
newFile = 'newarticle.txt' 

## Remove this substring
badSubString = ' (solution)' 

## Replace it with this substring
goodSubString = '' 


# Logic
def reading():
    with open(oldFile, "r") as f:
        for line in f.readlines():
            new_line = line.replace(badSubString, goodSubString)
            output = open(newFile, 'a')
            output.write("* " + new_line)

reading()
