# Format list articles for Lazy Wiki

* Save a list as a file, and bullet.py will format it for LazyWiki
* Perfect for long lists with undesired characters (extra spaces, punctuation, links, etc)

It will...

* Add bullets to each line
* Remove undesired substrings
* Replace substrings

# To Use

* Make a 'article.txt' in this directory
* Paste in it the article text
* Open 'bullet.py' and reassign the variables to suit your needs
* Assign the substring you want removed from article lines to badSubString
* Assign goodSubString to '' to remove the badSubString. To replace it, assign the replacement text to goodSubString instead.
* Run the program (maybe open terminal, cd here, python bullet.py)
* Copy the properly formatted list from the new 'newarticle.txt'
* Paste it in LazyWiki
* Empty or delete 'newarticle.txt' before using again

# Runs in Python 2.7
